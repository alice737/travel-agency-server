import mongoose, {Schema} from 'mongoose';

const UserScheme = new Schema({
    password : String,
    name: String,
    surname: String,
    email: String,
    address: String,
    phoneNumber: Number,
    ordersIds: [Schema.Types.ObjectId],
    cartToursIds: [Schema.Types.ObjectId],
    role: String 
});

export default mongoose.model('User', UserScheme);
