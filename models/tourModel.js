import mongoose, {Schema} from 'mongoose';


const TourScheme = new Schema({
    imageSrc: {
        type: String
    },
    cityName: String,
    countryName:  String,
    hotelName:  String,
    description: String,
    price: Number,
    startDate: Date,
    endDate: Date,
    numberSeats:{
        type: Number
    }, 
    numberReservedSeats:{
        type: Number,
        default:0
    },
    starCapacity: Number,
    comments:[]
});

export default mongoose.model('Tour', TourScheme);
