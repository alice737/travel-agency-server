import mongoose, {Schema} from 'mongoose';

const OrderSchema = new Schema({
  
    email: String,
    date: Date,
    orderedToursIds:[[]],
    isCompleted :  Boolean,
});

export default mongoose.model('Order', OrderSchema);

