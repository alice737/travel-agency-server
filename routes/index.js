import tour from '../controllers/tourController';
import user from '../controllers/userController'
import order from '../controllers/orderController'

export default (app) => {
    app.route('/tours')
        .get(tour.getAllTours)
        .post(tour.createTour);

    app.route('/tours/:tourId')
        .get(tour.getTour)
        .put(tour.updateTour)
        .delete(tour.deleteTour);

    app.route('/register')
        .post(user.createUser)
        .get(user.getAllUsers);

    app.route('/getOne/:email')
        .get(user.getOneUser);

    app.route('/addToCart/:email')
        .post(user.addToCart);

    app.route('/login')
        .post(user.login);

    app.route('/order/:email')
        .post(order.createOrder)
        .get(order.getAllOrders)
        .get(order.getOrdersByUsername);

};