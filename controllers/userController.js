import user from '../models/users'
let jwt = require('jsonwebtoken');


exports.createUser = (req, res) => {
    const newUser = new user(req.body);
    newUser.save((err, user) => {
        if (err) {
            res.send(err);
        }

        res.json(user);
    });
};
exports.getAllUsers = (req, res) => {
    user.find({}, (err, users) => {
        if (err) {
            res.send(err);
        }

        res.json(users);
    });
};


exports.getOneUser = (req,res)=>{
    user.findOne({email : req.params.email}, (err, users) => {
        if (err) {
            res.send(err);
        }

        res.json(users);
    });
}

exports.addToCart= (req, res) => {
  
    user.findOneAndUpdate({email : req.params.email}, {$push : {cartToursIds : req.body._id}}, (err, user) => {
        if (err) {
            throw err;
        }
        res.json(user);
    
});
}

exports.login = (req, res) => {
    user.find({email: req.body.email, password: req.body.password},  (err, user) => {
        if (err || !user || !user.length) {
            res.status(401).json({
                title: 'Unable to log in',
                error: {message: 'Bad credentials'}
            });
        }
        else {
            console.log(user)
            const payload = {
                email: user.email,
                role : user.role
            };
            let token = jwt.sign(payload, req.app.get('privateKey'), {
                expiresIn: 144000
            });
            res.json({
                success: true,
                role : user[0].role,
                token: token
            });
            console.log(user[0].role)
        }
    });

    exports.checkUsername =(req, res) => {
   
        user.findOne({email : req.params.email}, (err, users) => {
            if (err) {
                throw err;
            }
            res.json(users);
        
    });

    

    }

    
}
