import order from '../models/order'

exports.createOrder = (req, res) => {
    const newOrder = new order(req.body);
    newOrder.save((err, order) => {
        if (err) {
            res.send(err);
        }

        res.json(order);
    });
};

exports.getAllOrders = (req, res) => {
    order.find({}, (err, orders) => {
        if (err) {
            res.send(err);
        }

        res.json(orders);
    });
};

exports.getOrdersByUsername = (req, res) => {
    order.findById(req.params.email, (err, order) => {
        if (err) {
            res.send(err);
        }
        res.json(order);
    });
};