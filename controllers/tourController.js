import tour from '../models/tourModel.js';

exports.getTour = (req, res) => {
    tour.findById(req.params.tourId, (err, tour) => {
        if (err) {
            res.send(err);
        }

        res.json(tour);
    });
};

exports.getAllTours = (req, res) => {
    tour.find({}, (err, tours) => {
        if (err) {
            res.send(err);
        }

        res.json(tours);
    });
};

exports.createTour = (req, res) => {
    const newTour = new tour(req.body);
    newTour.save((err, tour) => {
        if (err) {
            res.send(err);
        }

        res.json(tour);
    });
};

exports.updateTour = (req, res) => {
    tour.findOneAndUpdate({
        _id: req.params.tourId
    }, req.body,
        (err, tour) => {
            if (err) {
                res.send(err);
            }
            res.json(tour);
        });
};

exports.deleteTour = (req, res) => {
    tour.remove({
        _id: req.params.tourId
    }, (err) => {
        if (err) {
            res.send(err);
        }

        res.json({
            message: `Tour ${req.params.tourId} successfully deleted`
        });
    });
};